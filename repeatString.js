// Repeat a given string str (first argument) for num times
//  (second argument). Return an empty string if num is not a positive number.

function repeatStringNumTimes(str, num) {
    var result = "";
      if (num <= 0) {
        result = ""
      } else {
        for (var i = 1; i <= num; i += 1) {
            result += str;
        }
     }

    return result;
}



repeatStringNumTimes("abc", 3);


